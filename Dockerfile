FROM bioconductor/bioconductor_docker:RELEASE_3_11

# deps from cran
RUN install2.r --error effsize ggpubr gplots ggplot2 gridGraphics gridExtra RColorBrewer reshape2 robCompositions vegan xlsx permute zCompositions 
RUN install2.r --error roxygen2 rmarkdown

# deps from bioconductor
RUN Rscript -e 'requireNamespace("BiocManager"); BiocManager::install(c("phyloseq", "pcaMethods", "edgeR", "DESeq2"));'

# deps from github
RUN Rscript -e 'library(devtools); install_github("ikwak2/DrImpute"); install_github("Vivianstats/scImpute", ref = "47c843d");'

# deps from other sources
COPY ./docker/deps deps 
RUN Rscript -e 'install.packages("deps/DiversitySeq_1.0.tar.gz")'
RUN Rscript -e 'install.packages("deps/GMPR_0.1.3.tar.gz")'
RUN rm -rf /tmp/downloaded_packages/ /tmp/*.rds

COPY main.R /home/rstudio/main.R
COPY README.md /home/rstudio/README.md

RUN chmod -R 777 /home/rstudio/main.R
RUN chmod -R 777 /home/rstudio/README.md

RUN echo build v.1.0.0-4
RUN Rscript -e 'Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS="true"); library(devtools); install_gitlab("sysbiobig/beware-to-ignore-the-rare-imputation-16s", build_opts = c("--no-resave-data", "--no-manual"), build_vignettes = FALSE);'
