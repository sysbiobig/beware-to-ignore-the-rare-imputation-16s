#' Beware to ignore the rare: how imputing zero-values can improve the quality of 16S rRNA gene studies results.
#'
#' This is the R package containing the code and the data used to obtain the results shown in
#' Baruzzo G., Patuzzi I., Di Camillo B. "Beware to ignore the rare: how imputing zero-values can improve the quality of 16S rRNA gene studies results." (2020).
#' This work assesses the role of zero-imputation and normalization on 16S rDNA-seq count table,
#' evaluating the different pre-processing strategies by means of quantitative metrics and the effect on downstream analyses.
#'
#'
#' @docType package
#'
#' @author Giacomo Baruzzo \email{giacomo.baruzzo@@unipd.it}
#' @author Ilaria Patuzzi \email{ilaria.patuzzi@@eubiome.it}
#' @author Barbara Di Camillo \email{barbara.dicamillo@@unipd.it}
#'
#' @importFrom grDevices dev.list pdf svg
#' @importFrom graphics par abline boxplot text
#' @importFrom stats median sd p.adjust wilcox.test
#' @importFrom utils combn
#'
#' @name bewareToIgnoreTheRareImputation16s
#'
NULL

