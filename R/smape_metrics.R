# Calculates the SMAPE between two vector.
# param:
#   - true_vector: a vector of ground truth proportions
#   - predicted_vector: a vector of predicted proportions
# return: The value of SMAPE between the two vector provided in input.
smape<-function (true_vector, predicted_vector){
  smape_V<-vector()
  for (i in (1:length(true_vector))){
    if (!((true_vector[i]==0)&(predicted_vector[i]==0))){
      smape_V[i]<-abs(true_vector[i]-predicted_vector[i])/(abs(true_vector[i]) + abs(predicted_vector[i]))
    } else{
      smape_V[i]<-0
    }
  }
  return(100*mean(smape_V))
}

#' SMAPE
#'
#' This function calculates SMAPE metrics between each column of the Ground Truth and one or more Pre-processed matrices.
#'
#' @param gt_matrix Ground Truth OTU table (OTUs on rows, samples on columns)
#' @param pp_matrix a matrix or a list of Pre-processed OTU table (OTUs on rows, samples on columns). If 'pp_matrix' is a list of OTU table the SMAPE is calculated between 'gt_matrix' and each element.
#' @return a matrix containing the SMAPE values between each column of 'gt_matrix' and 'pp_matrix' on rows for each Pre-processed matrices provide as input on column.
#' @export
calculate_smape<- function(gt_matrix, pp_matrix){
  # Check input:
  if(!is.matrix(gt_matrix)){
    stop("Argument 'gt_matrix' must be a matrix of OTU table")
  }
  if(!(is.matrix(pp_matrix)|is.list(pp_matrix))){
    stop("Argument 'pp_matrix' must be a matrix of OTU table or a list of OTU table")
  }
  # If pp_matrix is a single matrix create a list of a single element:
  if(!is.list(pp_matrix)){
    pp_matrix<- list(pp_matrix)
    names(pp_matrix)<- "pp_matrix"
  }
  # If pp_matrix is a list not named:
  if(is.list(pp_matrix)&is.null(names(pp_matrix))){
    names(pp_matrix)<- paste0("pp_matrix_", c(1:length(pp_matrix)))
  }
  # If samples in gt_matrix are not named:
  if(is.null(colnames(gt_matrix))){
    colnames(gt_matrix)<- paste0("s_", c(1:ncol(gt_matrix)))
  }
  # Initialize the output matrix:
  smape_matrix<- matrix(data = 0, nrow = ncol(gt_matrix), ncol = length(pp_matrix))
  # Calculate SMAPE:
  for (i in 1:length(pp_matrix)){
    if(any(dim(gt_matrix)!=dim(pp_matrix[[i]]))){
      stop("The dimension of the matrices compared must be equal. Please check the dimension.")
    }
    for (j in (1:ncol(gt_matrix))){
      smape_matrix[j,i]<- smape(gt_matrix[,j],pp_matrix[[i]][,j])
    }
  }
  rownames(smape_matrix)<- colnames(gt_matrix)
  colnames(smape_matrix)<- names(pp_matrix)
  return(smape_matrix)
}

#' Effect size test
#'
#' This function test for improvement in abundance profiles results between no (raw data named "Raw") and different Pre-Processing pipelines.
#' From all the sample, a one-sided Mann-Whitney test is performed between the SMAPE/Aitchison distance on raw data and the SMAPE/Aitchison distance on Pre-processed OTU table.
#' This function also calculates the effect size to measure the magnitude of possible significant differences between distributions.
#'
#' @param measure_matrix a matrix of SMAPE/Aitchison distance values where rows correspond to samples and columns to each Pre-processed OTU table (including Raw data, the first column).
#' @return a data.frame with the corrected p_values (p_value_corr), the results of the Mann-Whitney test (stat_sign), the effect size value (eff_size), the magnitude of the effect size (eff_size_res) on columns and each Pre-processed OTU table on rows.
#' @export
effect_size_test_abundance <- function (measure_matrix){
  p_DA<-vector()
  for(i in (2:ncol(measure_matrix))){
    p_DA[i-1]<-suppressWarnings(wilcox.test(round(measure_matrix[,i], digits=10),
                                            round(measure_matrix[,1], digits=10),
                                            alternative = "less",paired = TRUE,exact = NULL)$p.value)
  }
  names(p_DA)<-colnames(measure_matrix)[-1]
  p_value_corr<-p.adjust(p_DA, method = "BH")
  stat_sign<-ifelse(p_value_corr<0.05,"Yes","No")

  #Effect size
  eff_size<-vector()
  eff_size_res<-vector()
  for (i in (2: dim(measure_matrix)[2])){
    eff_size[i-1]<-round(effsize::cohen.d(measure_matrix[,1],measure_matrix[,i],hedges.correction = T,pooled=T)$estimate, digits=12)
    eff_size_res[i-1] <- effect_size_symbol(eff_size[i-1])
  }
  eff_sig<-data.frame(p_value_corr,stat_sign,eff_size,eff_size_res)
  rownames(eff_sig)<-colnames(measure_matrix)[-1]
  return(eff_sig)
}


#' Effect size test
#'
#' This function test for improvement in abundance profiles results between no (raw data named "Raw") and different Pre-Processing pipelines.
#' From all the sample, a one-sided Mann-Whitney test is performed between the SMAPE/Aitchison distance on raw data and the SMAPE/Aitchison distance on Pre-processed OTU table.
#' This function also calculates the effect size to measure the magnitude of possible significant differences between distributions.
#'
#' @param data The ouput of \code{calculate_smape()} or \code{calculate_aitchison_dist()}. A matrix of SMAPE/Aitchison's distance values where rows correspond to samples and columns to each Pre-processed OTU table (including Raw data, the first column).
#' @param data_stat Statistics about the values contained in \code{data}. It is the output of \code{effect_size_test_abundance()} apply to \code{data}.
#' @param dataset_name Name of the analized dataset. It will be shown in the plot title
#' @param measure_name Name of the measure/distance used (i.e. SMAPE or Aitchison's distance). It will be shown in the plot title
#' @param ymax Graphical parameters to set the max value of the SMAPE/Aitchison's distance axis
#' @param pval_label_pos Graphical parameters to set the positition of the "*" symbol for statistically significant boxplots (i.e. pipelines)
#' @param effsize_label_pos Graphical parameters to set the positition of the effect size magnitude for statistically significant boxplots (i.e. pipelines)
#' @return None. The function print the boxplots obtained from the input data.
#' @export
composition_boxplot <- function(data, data_stat, dataset_name, measure_name,
                                ymax = 100, pval_label_pos = 100, effsize_label_pos=120){

  pipeline_groupby_imp <- c(
    "Raw", "TSS", "CSS", "edgeR", "DESeq2", "GMPR",
    "None_DrImpute", "TSS_DrImpute", "CSS_DrImpute", "edgeR_DrImpute", "DESeq2_DrImpute", "GMPR_DrImpute",
    "None_scImpute", "TSS_scImpute", "CSS_scImpute", "edgeR_scImpute", "DESeq2_scImpute", "GMPR_scImpute",
    "None_LLSimpute", "TSS_LLSimpute", "CSS_LLSimpute", "edgeR_LLSimpute", "DESeq2_LLSimpute", "GMPR_LLSimpute",
    "None_zCompositions_SQ", "TSS_zCompositions_SQ", "CSS_zCompositions_SQ", "edgeR_zCompositions_SQ", "DESeq2_zCompositions_SQ", "GMPR_zCompositions_SQ",
    "None_zCompositions_CZM", "TSS_zCompositions_CZM", "CSS_zCompositions_CZM", "edgeR_zCompositions_CZM", "DESeq2_zCompositions_CZM", "GMPR_zCompositions_CZM"
  )
  pipeline_groupby <- pipeline_groupby_imp

  data <- data[,rev(pipeline_groupby)]

  ## add fake entry for raw data (raw data have no pvalue and effect size since they are the baseline)
  tmp <- as.data.frame(t(c(1,"No",0,"N"))); rownames(tmp) <- "Raw"; colnames(tmp) <- colnames(data_stat)
  data_stat <- rbind(data_stat, tmp)
  data_stat <- data_stat[rev(pipeline_groupby),]

  # entries having the "*" symbol
  sign_data <- data_stat$stat_sign=="Yes"

  # graphical parameters
  par(mar = c(4, 13, 2, 2)+ 0.1)
  boxplot_ycoord <- c(1,2,3,4,5,6, 8,9,10,11,12,13, 15,16,17,18,19,20,
                      22,23,24,25,26,27, 29,30,31,32,33,34, 36,37,38,39,40, 42)
  col_palette <- rev(c("grey", rep("red", 5), rep("cyan", 6), rep("green", 6),
                       rep("yellow", 6), rep("orange", 6), rep("blue",6)))

  # boxplots
  boxplot(data, main = paste0(dataset_name, " - ", measure_name), las = 2, horizontal = TRUE,
          ylim = c(0,ymax), at = boxplot_ycoord, col = col_palette    )
  abline(h=boxplot_ycoord, col = "grey60", lty = 2)
  par(new=TRUE)
  boxplot(data, main = paste0(dataset_name, " - ", measure_name), las = 2, horizontal = TRUE,
          ylim = c(0,ymax), at = boxplot_ycoord, col = col_palette    )

  # baseline line and labels
  abline(v=median(data[,"Raw"]), col = "black", lty = 5)
  if(sum(sign_data)>0){
    text(pval_label_pos, boxplot_ycoord[sign_data], "*", cex = 2)
    text(effsize_label_pos, boxplot_ycoord[sign_data], data_stat$eff_size_res[sign_data], cex = 1, pos = 4)
  }
}
