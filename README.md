# Beware to ignore the rare: how imputing zero-values can improve the quality of 16S rRNA gene studies results

This is the repository containing the code and the data used to obtain the results shown in *Baruzzo G., Patuzzi I., Di Camillo B. "Beware to ignore the rare: how imputing zero-values can improve the quality of 16S rRNA gene studies results." (2021)*.

This work assesses the role of zero-imputation and normalization on 16S rDNA-seq count table, evaluating the different pre-processing strategies by means of quantitative metrics and the effect on downstream analyses. A total of 35 pre-processing pipelines (5 normalization pipelines, 5 zero-imputation pipelines and 25 normalization+imputation pipelines) has been tested on 3 test scenarios.

The repository contains the R package **bewareToIgnoreTheRareImputation16s**; the R package contains the data and the code to reproduce the findings about

* total sparsity
* species presence/absence
* relative abundance profile (SMAPE error and Aitchison distance)
* impact on bacterial diversity (alpha and beta diversities)
* differential abundance analysis

using the ground truth and raw data (i.e. [metaSPARSim](https://gitlab.com/sysbiobig/metasparsim) output) and the pre-processed data with the 35 tested pipelines.

The repository contains also the Docker image **beware-to-ignore-the-rare-imputation-16s**; the Docker image contains the R package **bewareToIgnoreTheRareImputation16s** and the methods for count normalization and zero-imputation used in the study.

## How to install

**bewareToIgnoreTheRareImputation16s** is an R (https://www.r-project.org/) package available on GitLab at https://gitlab.com/sysbiobig/beware-to-ignore-the-rare-imputation-16s

To install **bewareToIgnoreTheRareImputation16s** from GitLab, please use the following commands:

```r
Sys.setenv(R_REMOTES_NO_ERRORS_FROM_WARNINGS="true")
library(devtools)
install_gitlab("sysbiobig/beware-to-ignore-the-rare-imputation-16s", build_opts = c("--no-resave-data", "--no-manual"), build_vignettes = FALSE)
```

## How to run 

Open a R session and load the library using the command:

```r
library(bewareToIgnoreTheRareImputation16s)
```
then follow these steps:

1. Download **`main.R`** from [here](https://gitlab.com/sysbiobig/beware-to-ignore-the-rare-imputation-16s/-/blob/master/main.R?inline=false);
2. Run to reproduce the plots and tables shown in manuscript and supplementary materials.

## Alternative: How to install & run using Docker container

A Docker (https://www.docker.com/) container is available at https://gitlab.com/sysbiobig/beware-to-ignore-the-rare-imputation-16s/container_registry/

Open a terminal and start the Docker container using the command:

```r
docker run --name beware-to-ignore-the-rare-imputation-16s -p 127.0.0.1:8787:8787 -d -e PASSWORD=imputation registry.gitlab.com/sysbiobig/beware-to-ignore-the-rare-imputation-16s:1.0.0
```

then open browser on **localhost:8787** to work with Rstudio inside the container image (log in with **username** `rstudio` and **password** `imputation`).

The **beware-to-ignore-the-rare-imputation-16s** Docker image has already installed the library **bewareToIgnoreTheRareImputation16s** and it contains the **`main.R`** file in the `/home/rstudio/main.R` directory.

Then you are ready to run **`main.R`** to reproduce the plots and tables shown in manuscript and supplementary materials.

All the files created by **`main.R`** are in results folder inside the container, you can download from **more -> export** button in RStudio.

#### Using container with local folder
If you want to run container and bind your local folder to load your custom data or access result files in your local machine you can start container in this way:

1. Create results folders in `/YOUR-PATH/results`, where `/YOUR-PATH/results` is the local path to mount inside the container in `/home/rstudio/results`;

2. All the files you want to see in the container must be in `/YOUR-PATH/results`;

3. Run the following command 
```r
    docker run --name beware-to-ignore-the-rare-imputation-16s -p 127.0.0.1:8787:8787 -d -v /YOUR-PATH/results:/home/rstudio/results -e PASSWORD=imputation registry.gitlab.com/sysbiobig/beware-to-ignore-the-rare-imputation-16s:1.0.0
```

4. Open browser on **localhost:8787** to work with Rstudio inside the container image (log in with **username** `rstudio` and **password** `imputation`).  


## File main.R
The **`main.R`** file contains the code to obtain the results shown in *Baruzzo G., Patuzzi I., Di Camillo B. "Beware to ignore the rare: how imputing zero-values can improve the quality of 16S rRNA gene studies results" (2021)*. \
The code is structured in several sections:

* **SECTION 1: INPUT PREPARATION** This section loads the data and prepares the input variables for the analyses performed in the following sections. The user must specify one of the three available datasets on which to run the code. The user can decide to run only one or more of the sections of the main.R, but this section must always be run first.

* **SECTION 2: TOTAL SPARSITY** This section compares the total sparsity between Ground Truth, Raw and pre-processed data. 

* **SECTION 3: SPECIES PRESENCE/ABSENCE** This section assess the ability of the pipelines in detecting truly present and absent species (i.e. technical zeros vs biological zeros). 

* **SECTION 4: RELATIVE ABUNDACE PROFILE** This section compares the relative abundace of Ground Truth, Raw and pre-processed data. (SMAPE and Aitchison's distance).

* **SECTION 5: IMPACT ON BACTERIA DIVERSITY** This section assesses the ability of the pipelines in recostructing the Ground Truth in terms of Alpha and Beta diversity indices.

* **SECTION 6: DIFFERENTIAL ABUNDANCE** This section assess the ability of the pipelines in improving the results of differential abundance analysis.


## Dataset
Three simulated datasets were selected for benchmarking. [metaSPARSim](https://gitlab.com/sysbiobig/metasparsim) [Patuzzi et al., BMC Bioinformatics, 2019] simulator was used to generate three datasets resembling 3 existing real datasets. For each dataset, the output of metaSPARSim simulator is the Ground Truth and the Raw data. Then, the Raw data were input to 5 normalization-only, 5 zero-imputation-only and 25 pipelines combining the 5 normalization methods and the 5 zero-imputation methods. 


In the **beware-to-ignore-the-rare-imputation-16s** package the user can load the Ground Truth, the Raw and the Pre-processed data for each of the 3 test datasets, named 
`dataset1`, `dataset2` and `dataset3`. For example, to load the Ground Truth, the Raw and the Pre-processed data of dataset2, the user can use the command `data(dataset2)`.

Each dataset contains a list with the following elements:

- **res** a list containing the Ground Truth, the Raw data and all the 35 Pre-processed data.
- **res_prop** a list containing the Ground Truth, the Raw data and all the 35 Pre-processed data where the abundances of the features have been transformed into Count Per Millions (CPM).
- **group** a factor vector which contains the labels of the group to which each subject belongs.
